#!/bin/sh

# ed(1) helper script to alter settings in the helper scripts.
# Author -
# Usage: see usage() function, below

base_file=$(basename "$0")

usage(){
  print "ed(1) helper script to alter settings in the helper scripts."
  print ""
  print "Usage:"
  print "!$base_file [-m marker] [-n|-s]"
  print ""
  print "tmp script options:"
  print "   -c  The tmp script writes to the clipboard instead of a file."
  print ""
  print "trfm_ scripts options:"
  print "   -m  Set a marker string that does not appear in your file,"
  print "       and does not contain ed(1) command characters."
  print ""
  print "ctags_peek script options:"
  print "   -n  Turn line numbers on for ctags_peek."
  print "   -s  Turn syntax highlighting on for ctags_peek."
  print ""
  print "xdotool sleep time:"
  print "   -x  Gives you enough time to take your hands off the"
  print "       keyboard before xdotool starts typing."
}

# Default value for the marker.
trfm_marker="aSGHgaITOad"
line_numbers_on=""
syntax_highlighting_on=""

# http://www.andre-simon.de/index.php
# For themes: /usr/local/share/highlight/themes

while getopts "cm:nsx:h" arg
do
  case $arg in
    c)
      tmp_is_clipboard="yes"
      ;;
    m)
      trfm_marker=$OPTARG
      ;;
    n)
      line_numbers_on="yes"
      ;;
    s)
      syntax_highlighting_on="yes"
      ;;
    x)
      xdotool_sleep_time=$OPTARG
      ;;
    h|*)
      usage
      exit 1
      ;;
  esac
done

shift $((OPTIND - 1))

ed -s "$HOME/bin/ed/rled" <<EOF
H
g!\(export g_trfm_marker=\).*! s!!\1$trfm_marker!n
g!\(export g_ctags_line_nums_on=\).*! s!!\1$line_numbers_on!n
g!\(export g_syntax_highlighting_on=\).*! s!!\1$syntax_highlighting_on!n
g!\(export g_xdotool_sleep_time=\).*! s!!\1$xdotool_sleep_time!n
w
EOF

ed -s "$HOME/bin/ed/tmp" <<EOF
H
g!\(tmp_is_clipboard=\).*! s!!\1$tmp_is_clipboard!n
w
EOF
