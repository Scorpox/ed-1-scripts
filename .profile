# $OpenBSD: dot.profile,v 1.5 2018/02/02 02:29:54 yasuoka Exp $
#
# sh/ksh initialization

PATH=$HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin:/usr/games

PATH=$PATH:$HOME/bin/cscope
PATH=$PATH:$HOME/bin/ed
export PATH HOME TERM

export EDITOR="rled"
export PAGER="less"
#export ENV=$HOME/.kshrc
#export TERMINAL=xterm
export TERM=xterm-256color

PS1='λ '
# PS1='⍦ '

export FZF_DEFAULT_OPTS='--color=16'

export g_highlight_out_format=truecolor
export g_highlight_base16=solarized-dark

# An alias is a way of shortening a command. (They are only used in
# interactive shells and not in scripts — this is one of the very
# few differences between a script and an interactive shell.)

# If you don't want to write the leading "." each time you want to
# source your script to the shell environment, make an alias.
# Changing directory will have no effect without this.
alias cdg=". cdg"
alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'
# alias ranger='. ranger'


# Search the man pages for every occurrence of a word
man_search() {
  man -k any="$1"
}
